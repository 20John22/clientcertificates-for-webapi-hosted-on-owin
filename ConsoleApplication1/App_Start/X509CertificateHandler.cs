using System;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;


namespace ConsoleApplication1.App_Start
{
	public class X509CertificateHandler : DelegatingHandler
	{
		public Func<X509Certificate2, bool> ValidateCertificate { get; set; }

		public X509CertificateHandler(Func<X509Certificate2, bool> validateCertificate)
		{
			this.ValidateCertificate = validateCertificate;
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			var cert = request.GetClientCertificate();
			if (cert == null)
				throw new NotSupportedException("Certificate must be available.");

			//if (!HttpContext.Current.Request.ClientCertificate.IsPresent)
			//{
			//    return Task.Factory.StartNew(() => request.CreateResponse(HttpStatusCode.Unauthorized));
			//}

			//var cert = new X509Certificate2(HttpContext.Current.Request.ClientCertificate.Certificate);
			if (!this.ValidateCertificate(cert))
			{
				return Task.Factory.StartNew(() => request.CreateResponse(HttpStatusCode.Unauthorized));
			}

			return base.SendAsync(request, cancellationToken);
		}
	}
}