﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Http;
using ConsoleApplication1.App_Start;
using Newtonsoft.Json.Serialization;
using Owin;

namespace ConsoleApplication1
{
	class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			app.Use((env, task) =>
			{
				X509Certificate2 cert = env.Get<X509Certificate2>("ssl.ClientCertificate");
				string certErrors = env.Get<string>("ssl.ClientCertificateErrors");

				Func<Task> loadCert = env.Get<Func<Task>>("ssl.LoadClientCertAsync");
				loadCert();

			

				return task();
			});

			HttpConfiguration config = new HttpConfiguration();
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			config.MessageHandlers.Add(
				new X509CertificateHandler(
					cert =>
					{
						return cert.Thumbprint.Equals("678364F81A6E492B35A89A028E631F8CB6818AD3", System.StringComparison.OrdinalIgnoreCase);
					}));

			var jsonSettings = config.Formatters.JsonFormatter.SerializerSettings;
			jsonSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
			jsonSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

			app.UseWebApi(config);
		}
	}
}
