﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace ConsoleApplication1
{
	class Program
	{
		//RUN below command as admin in console and replace certhash b563fdc1176b7ffd0561a61cd2b0412c4b05a112 with thumprint for your server certificate: 
		//netsh http add sslcert ipport=0.0.0.0:44456 certhash=b563fdc1176b7ffd0561a61cd2b0412c4b05a112 appid={37255068-503b-48f4-afe8-0a576c548eea} clientcertnegotiation=enable
		static void Main(string[] args)
		{
			const string baseUrl = "https://localhost:44456/";
			//var wrapper = new OwinHttpListener();
			//wrapper.Start(wrapper.Listener, async env =>
			//{
			//	object obj;
			//	env.TryGetValue("ssl.LoadClientCertAsync", out obj);
			//	var loadCert = (Func<Task>)obj;
			//	await loadCert();
			//	env.TryGetValue("ssl.ClientCertificate", out obj);
			//}, CreateAddress(addressParts), null, null);

			using (WebApp.Start<Startup>(baseUrl))
			{
				Console.WriteLine("Press Enter to quit.");
				ClientCall(baseUrl);
				Console.ReadKey();
			}


		}

		private static void ClientCall(string baseUrl)
		{
			var clientCertificate = new X509Certificate2(File.ReadAllBytes(@"..\..\Certs\JohnDoe.pfx"), "THE_PASSWORD_USED");
			var client = WebRequest.Create(baseUrl) as HttpWebRequest;
			client.ClientCertificates.Add(clientCertificate);

			RemoteCertificateValidationCallback callback = delegate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors sslError)
			{
				X509Certificate2 clientCert = new X509Certificate2(cert);
				return true;
			};

			client.ServerCertificateValidationCallback += callback;
			string response = new StreamReader(client.GetResponse().GetResponseStream()).ReadToEnd();

			//var client = WebRequest.Create(baseUrl + "api/hello") as HttpWebRequest;
			//client.ClientCertificates.Add(cert);
			//string response = new StreamReader(client.GetResponse().GetResponseStream()).ReadToEnd();
		}
	}
}
